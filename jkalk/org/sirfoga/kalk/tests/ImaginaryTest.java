/*
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.sirfoga.kalk.tests;


import org.sirfoga.kalk.model.Imaginary;

public class ImaginaryTest extends Test {
    @Override
    public void testAll() {
        System.out.println("Running ImaginaryTest() tests...");

        Imaginary x = new Imaginary(2);
        Imaginary y = new Imaginary(4);
        test(x.divide(y).realVal() == 0.5);
        test(x.multiply(y).realVal() == 8);
    }

}
