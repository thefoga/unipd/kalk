/*
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.sirfoga.kalk.tests;

import org.sirfoga.kalk.model.Complex;

public class ComplexTest extends Test {
    @Override
    public void testAll() {
        System.out.println("Running ComplexTest() tests...");

        Complex x = new Complex(2, 3);
        Complex y = new Complex(4, 5);
        Complex z = x.multiply(y);
        test(z.realVal() == 8 - 15);
    }
}
