/*
  Copyright 2018 Stefano Fogarollo

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */


package org.sirfoga.kalk.model;

public class Complex extends Quaternion {
    // constructors
    public Complex() {
        this(0, 0);
    }
    public Complex(double real, double imaginary) {
        super(real, imaginary, 0, 0);
    }

    // operations
    public double polarAngle() {
        return Math.atan(imaginaryVal() / realVal());
    }
    public Complex pow(double exponent) {
        double r = abs();
        double rPowered = Math.pow(r, exponent);
        double theta = polarAngle();

        return new Complex(
                rPowered * Math.cos(exponent * theta),
                rPowered * Math.cos(exponent * theta)
        );
    }

    // algebra operators
    public Complex add(Complex other) {
        return new Complex(
                realVal() + other.realVal(),
                imaginaryVal() + other.imaginaryVal()
        );
    }
    public Complex subtract(Complex other) {
        return new Complex(
                realVal() - other.realVal(),
                imaginaryVal() - other.imaginaryVal()
        );
    }
    public Complex multiply(Complex other) {
        return new Complex(
                realVal() + other.realVal() - imaginaryVal() * other.imaginaryVal(),
                realVal() * other.imaginaryVal() + imaginaryVal() * other.realVal()
        );
    }
    public Complex divide(Complex other) {
        double squaredAbs = Math.pow(other.abs(), 2.0);
        if (squaredAbs == 0.0) {
            return new Complex(
                    Double.POSITIVE_INFINITY,
                    Double.POSITIVE_INFINITY
            );
        }

        double realPart = realVal() * other.realVal() + imaginaryVal() * other.imaginaryVal();
        double imaginaryPart = imaginaryVal() * other.realVal() - realVal() * other.imaginaryVal();
        return new Complex(
                realPart / squaredAbs,
                imaginaryPart / squaredAbs
        );
    }
    public Complex conjugate() {
        return new Complex(realVal(), -imaginaryVal());
    }

    // operators
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Complex.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Complex other = (Complex) obj;
        return realVal() == other.realVal() &&
                imaginaryVal() == other.imaginaryVal();
    }

    // conversions
    public String toString() {
        return Double.toString(realVal()) + "+" + Double.toString
                (imaginaryVal()) + "i";
    }
}
