/*
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.sirfoga.kalk.model;

public class Real extends Complex {
    // constructors
    public Real() {
        this(0);
    }
    public Real(double value) {
        super(value, 0);
    }

    // operations
    public Real pow(double exponent) {
        return new Real(Math.pow(realVal(), exponent));
    }
    public Real reciprocal() {
        return new Real(Math.pow(realVal(), -1.0));
    }

    // algebra operators
    public Real add(Real other) {
        return new Real(realVal() + other.realVal());
    }
    public Real subtract(Real other) {
        return new Real(realVal() - other.realVal());
    }
    public Real multiply(Real other) {
        return new Real(realVal() * other.realVal());
    }
    public Real divide(Real other) {
        if (this == other) {
            return new Real(1.0);
        }

        if (other.realVal() == 0.0) {
            return new Real(Double.POSITIVE_INFINITY);
        }

        return new Real(realVal() / other.realVal());
    }

    // operators
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Real.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Real other = (Real) obj;
        return realVal() == other.realVal();
    }

    // conversions
    public String toString() {
        return Double.toString(realVal());
    }
}
