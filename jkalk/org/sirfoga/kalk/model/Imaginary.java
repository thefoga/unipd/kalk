/*
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.sirfoga.kalk.model;

public class Imaginary extends Complex {
    // constructors
    public Imaginary() {
        this(0);
    }
    public Imaginary(double imaginary) {
        super(0, imaginary);
    }

    // algebra operators
    public Complex multiply(Complex other) {
        return new Complex(imaginaryVal() * other.imaginaryVal(), 0);
    }
    public Complex divide(Complex other) {
        return new Complex(imaginaryVal() / other.imaginaryVal(), 0);
    }

    // operators
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Imaginary.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Imaginary other = (Imaginary) obj;
        return imaginaryVal() == other.imaginaryVal();
    }

    // conversions
    public String toString() {
        return Double.toString(imaginaryVal()) + "i";
    }
}
