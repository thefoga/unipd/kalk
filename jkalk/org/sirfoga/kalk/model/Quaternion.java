/*
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.sirfoga.kalk.model;

public class Quaternion {
    // class variables
    private double realValue, imaginaryValue, jValue, kValue;

    // constructors
    public Quaternion() {
        this(0, 0, 0, 0);
    }
    public Quaternion(double real, double i, double j, double k) {
        setRealValue(real);
        setImaginaryValue(i);
        setJValue(j);
        setKValue(k);
    }

    // getters
    public double realVal() {
        return realValue;
    }
    public double imaginaryVal() {
        return imaginaryValue;
    }
    public double jVal() {
        return jValue;
    }
    public double kVal() {
        return kValue;
    }

    // setters
    public void setRealValue(double x) {
        realValue = x;
    }
    public void setImaginaryValue(double x) {
        imaginaryValue = x;
    }
    public void setJValue(double x) {
        jValue = x;
    }
    public void setKValue(double x) {
        kValue = x;
    }
    
    // operations
    public void zero() {
        setRealValue(0);
        setImaginaryValue(0);
        setJValue(0);
        setKValue(0);
    }
    public Quaternion conjugate() {
        return new Quaternion(realVal(), -imaginaryVal(), -jVal(), -kVal());
    }
    public double abs() {
        return Math.pow(
                Math.pow(realVal(), 2) +
                Math.pow(imaginaryVal(), 2) +
                Math.pow(jVal(), 2) +
                Math.pow(kVal(), 2), 0.5
        );
    }
    public Quaternion pow(double x) {
        int exponent = (int) Math.round(x);
        Quaternion result = new Quaternion(1, 1, 1, 1);
        for (int i = 0; i < exponent; i++) {
            result = result.multiply(this);
        }
        return result;
    }

    // algebra operators
    public Quaternion add(Quaternion other) {
        return new Quaternion(
                realVal() + other.realVal(),
                imaginaryVal() + other.imaginaryVal(),
                jVal() + other.jVal(),
                kVal() + other.kVal()
        );
    }
    public Quaternion subtract(Quaternion other) {
        return new Quaternion(
                realVal() - other.realVal(),
                imaginaryVal() - other.imaginaryVal(),
                jVal() - other.jVal(),
                kVal() - other.kVal()
        );
    }
    public Quaternion multiply(Quaternion other) {
        return new Quaternion(
                other.realVal() * realVal() - other.imaginaryVal() * imaginaryVal() - other.jVal() * jVal() - other.kVal() * kVal(),
                other.realVal() * imaginaryVal() + other.imaginaryVal() * realVal() - other.jVal() * kVal() + other.kVal() * jVal(),
                other.realVal() * jVal() + other.imaginaryVal() * kVal() + other.jVal() * realVal() - other.kVal() * imaginaryVal(),
                other.realVal() * kVal() - other.imaginaryVal() * jVal() + other.jVal() * imaginaryVal() + other.kVal() * realVal()
        );
    }
    public Quaternion divide(Quaternion other) {
        double squaredAbs = Math.pow(other.abs(), 2.0);
        if (squaredAbs == 0.0) {
            return new Quaternion(
                    Double.POSITIVE_INFINITY,
                    Double.POSITIVE_INFINITY,
                    Double.POSITIVE_INFINITY,
                    Double.POSITIVE_INFINITY
            );
        }

        double realPart = other.realVal() * realVal() + other.imaginaryVal() * imaginaryVal() + other.jVal() * jVal() + other.kVal() * kVal();
        double iPart = other.realVal() * imaginaryVal() - other.imaginaryVal() * realVal() - other.jVal() * kVal() + other.kVal() * jVal();
        double jPart = other.realVal() * jVal() + other.imaginaryVal() * kVal() - other.jVal() * realVal() - other.kVal() * imaginaryVal();
        double kPart = other.realVal() * kVal() - other.imaginaryVal() * jVal() + other.jVal() * imaginaryVal() - other.kVal() * realVal();
        return new Quaternion(
                realPart / squaredAbs,
                iPart / squaredAbs,
                jPart / squaredAbs,
                kPart / squaredAbs
        );
    }

    // comparisons
    public boolean lessThan(Quaternion other) {
        return abs() < other.abs();
    }
    public boolean greaterThan(Quaternion other) {
        return !this.equals(other) && !this.lessThan(other);
    }

    // operators
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Quaternion.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Quaternion other = (Quaternion) obj;
        return realVal() == other.realVal() &&
                imaginaryVal() == other.imaginaryVal() &&
                jVal() == other.jVal() &&
                kVal() == other.kVal();
    }

    // conversions
    public String toString() {
        return Double.toString(realVal()) +
                "+" + Double.toString(imaginaryVal()) + "i" +
                "+" + Double.toString(jVal()) + "j" +
                "+" + Double.toString(kVal()) + "k";
    }
}
