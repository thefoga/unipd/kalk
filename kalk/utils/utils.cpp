/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <algorithm>
#include <cctype>
#include <locale>
#include "utils.h"

// trim from start (in place)
void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

std::vector<std::string> split(std::string s, std::string delimiter) {
    size_t pos = 0;
    std::string token;
    std::vector<std::string> tokens;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        tokens.push_back(token);
        s.erase(0, pos + delimiter.length());
    }

    tokens.push_back(s);  // add last token
    return tokens;
}

bool contains(std::string container, std::string candidate) {
    return container.find(candidate) != std::string::npos;
}

std::string removeLast(std::string s) {
    return s.substr(0, s.size() -1);
}

double parseString(std::string s) {
    std::istringstream ss(s);
    double result;
    ss >> result;
    return result;
}

double fixValueEps(double x, double eps) {
    if (std::abs(x) < eps) {
        return 0.0;
    }

    return x;
}

bool areApproximatelyTheSame(double x, double y, double eps) {
    return std::abs(x - y) < eps;
}

double truncateDigits(double x, int n) {
    std::string output = std::to_string(x).substr(0, n + 1);
    if (output.find('.') ==  std::string::npos || output.back() == '.') {
        output.pop_back();
    }
    return parseString(output);
}
