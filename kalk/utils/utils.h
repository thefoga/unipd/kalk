/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>
#include <sstream>

// trim from start (in place)
void ltrim(std::string &s);

// trim from end (in place)
void rtrim(std::string &s);

// trim from both ends (in place)
void trim(std::string &s);

// trim from start (copying)
std::string ltrim_copy(std::string s);

// trim from end (copying)
std::string rtrim_copy(std::string s);

// trim from both ends (copying)
std::string trim_copy(std::string s);

std::vector<std::string> split(std::string s, std::string delimiter);

bool contains(std::string container, std::string candidate);

std::string removeLast(std::string);

template <class T>
int sizeOfArray(T array) {
    return sizeof(array) / sizeof(*array);
}

double parseString(std::string);

double fixValueEps(double, double);

bool areApproximatelyTheSame(double, double, double);

double truncateDigits(double, int);

#endif // UTILS_H
