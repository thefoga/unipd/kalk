/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef TESTS_COMPLEX_H
#define TESTS_COMPLEX_H

#include <iostream>
#include <vector>

#include "../model/complex.h"
#include "test.h"
#include "../utils/utils.h"

class ComplexTest : public Test
{
public:
    void testParsing() {
        std::string raw[] = {"5 + 6i", "8.99", "-9.0i", " 6i - 4", "- 9i+1 "};
        double reals[] = {5, 8.99, 0, -4, 1};
        double imags[] = {6, 0, -9.0, 6, -9};

        for(int i = 0; i < 5; i++) {
            Complex* c = new Complex(raw[i]);
            test(c->realVal() == reals[i]);
            test(c->imaginaryVal() == imags[i]);
        }
    }

    void testConstructor() {
        Complex x(2, 3), y(4, 5);
        Complex* z = x * y;
        test(z->realVal() == 8 - 15);

        Complex w("-1");
        test(*w.pow(0.5) == Complex(0, 1));
    }

    void testAll() {
        std::cout << "Running ComplexTest() tests..." << std::endl;

        testConstructor();
        testParsing();
    }
};

#endif
