/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <assert.h>

class Test
{
public:
    bool test(bool expression) const {
        try {
            assert(expression);
            return true;
        } catch (...) {
            std::cout << "[TEST FAILED]" << std::endl;
            return false;
        }
    }

    template <class T>
    bool assertTrue(T first, T second) const {
        return test(first == second);
    }

    template <class T>
    bool assertFalse(T first, T second) const {
        return !assertTrue(first, second);
    }

    virtual void testAll() = 0;  // to be implemented in each derived class
};

#endif
