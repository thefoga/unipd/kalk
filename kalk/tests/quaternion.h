/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef TESTS_QUATERNION_H
#define TESTS_QUATERNION_H

#include <iostream>

#include "../model/quaternion.h"
#include "test.h"

class QuaternionTest : public Test
{
public:
    void testParsing() {
        std::string raw[] = {"5 + 6i", "8k - 2j + 9 -2i", "- 9i + 8j - 2k +1"};
        double reals[] = {5, 9, 1};
        double is[] = {6, -2, -9};
        double js[] = {0, -2, 8};
        double ks[] = {0, 8, -2};

        for(int i = 0; i < 3; i++) {
            Quaternion* c = new Quaternion(raw[i]);
            test(c->realVal() == reals[i]);
            test(c->imaginaryVal() == is[i]);
            test(c->jVal() == js[i]);
            test(c->kVal() == ks[i]);
        }
    }

    void testConstructor() {
        Quaternion x(2, 3, 4, 5), y(4, 5, 6, 7);
        Quaternion* z = x * y;
        test(z->realVal() == -66);
        test(z->imaginaryVal() == 20);
        test(z->jVal() == 32);
        test(z->kVal() == 32);
    }

    void testAll() {
        std::cout << "Running QuaternionTest() tests..." << std::endl;

        testConstructor();
        testParsing();
    }
};

#endif
