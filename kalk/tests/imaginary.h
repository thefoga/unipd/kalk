/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef TESTS_IMAGINARY_H
#define TESTS_IMAGINARY_H

#include "../model/imaginary.h"
#include "test.h"

class ImaginaryTest : public Test
{
public:
    void testParsing() {
        std::string raw[] = {"6i", "6.99i", "-9i", " -1i", "- 8i "};
        double imags[] = {6, 6.99, -9, -1, -8};

        for(int i = 0; i < 5; i++) {
            Imaginary* c = new Imaginary(raw[i]);
            test(c->imaginaryVal() == imags[i]);
        }
    }

    void testConstructor() {
        Imaginary x(2), y(4);
        Real* div = x / y;
        Real* mul = x * y;

        test(*div == 0.5);
        test(*mul == 8);
    }

    void testAll() {
        std::cout << "Running ImaginaryTest() tests..." << std::endl;

        testConstructor();
        testParsing();
    }

};

#endif
