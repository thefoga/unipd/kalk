/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <cmath>
#include <iostream>  // todo debug

#include "button.h"
#include "kalk.h"

Kalk::Kalk(QWidget *parent) : QWidget(parent) {
    setupDisplay();
    clearAll();
    setupLayout();
}


void Kalk::setupDisplay() {
    display = new QLineEdit("0");
    display->setReadOnly(true);
    display->setAlignment(Qt::AlignRight);
    display->setMaxLength(32);
    QFont font = display->font();
    font.setPointSize(font.pointSize() + 8);
    display->setFont(font);
}

void Kalk::setupLayout() {
    iButton = createButton(tr("i"), SLOT(unaryOperatorClicked()));
    jButton = createButton(tr("j"), SLOT(unaryOperatorClicked()));
    kButton = createButton(tr("k"), SLOT(unaryOperatorClicked()));

    setLayout(createLayout());
    setWindowTitle(tr("Kalk"));
}

QGridLayout *Kalk::createLayout() {
    for (int i = 0; i < NumDigitButtons; ++i) {
        digitButtons[i] = createButton(QString::number(i), SLOT(digitClicked()));
    }

    Button *pointButton = createButton(tr("."), SLOT(pointClicked()));

    Button *backspaceButton = createButton(tr("Backspace"), SLOT(backspaceClicked()));
    Button *clearButton = createButton(tr("Clear"), SLOT(clear()));
    Button *clearAllButton = createButton(tr("Clear All"), SLOT(clearAll()));

    Button *divisionButton = createButton(tr("/"), SLOT(multiplicativeOperatorClicked()));
    Button *timesButton = createButton(tr("x"), SLOT(multiplicativeOperatorClicked()));
    Button *minusButton = createButton(tr("-"), SLOT(additiveOperatorClicked()));
    Button *plusButton = createButton(tr("+"), SLOT(additiveOperatorClicked()));

    squareRootButton = createButton(tr("Sqrt"), SLOT(unaryOperatorClicked()));
    powerButton = createButton(tr("x ^ y"), SLOT(powerOperatorClicked()));
    Button *reciprocalButton = createButton(tr("1/x"), SLOT(unaryOperatorClicked()));
    Button *equalButton = createButton(tr("="), SLOT(equalClicked()));

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);
    mainLayout->addWidget(display, 0, 0, 1, 6);
    mainLayout->addWidget(backspaceButton, 1, 0, 1, 2);
    mainLayout->addWidget(clearButton, 1, 2, 1, 2);
    mainLayout->addWidget(clearAllButton, 1, 4, 1, 2);

    for (int i = 1; i < NumDigitButtons; ++i) {
        int row = ((9 - i) / 3) + 2;
        int column = ((i - 1) % 3);
        mainLayout->addWidget(digitButtons[i], row, column);
    }

    mainLayout->addWidget(digitButtons[0], 5, 1);
    mainLayout->addWidget(pointButton, 5, 2);

    mainLayout->addWidget(plusButton, 2, 4);
    mainLayout->addWidget(minusButton, 3, 4);
    mainLayout->addWidget(timesButton, 4, 4);
    mainLayout->addWidget(divisionButton, 5, 4);

    mainLayout->addWidget(squareRootButton, 2, 5);
    mainLayout->addWidget(powerButton, 3, 5);
    mainLayout->addWidget(reciprocalButton, 4, 5);
    mainLayout->addWidget(equalButton, 5, 5);

    mainLayout->addWidget(iButton, 2, 3);
    mainLayout->addWidget(jButton, 3, 3);
    mainLayout->addWidget(kButton, 4, 3);

    iButton->setEnabled(false);
    jButton->setEnabled(false);
    kButton->setEnabled(false);

    QComboBox *combo = new QComboBox();
    combo->addItem("R");
    combo->addItem("C");
    combo->addItem("Q");
    connect(combo, SIGNAL(currentIndexChanged(int)), SLOT(setNumberDomain(int)));

    mainLayout->addWidget(combo, 5, 3);

    return mainLayout;
}

void Kalk::setNumberDomain(int domain) {
    if (domain == 0) {
        setRealDomain();
    } else if (domain == 1) {
        setComplexDomain();
    } else if (domain == 2) {
        setQuaternionDomain();
    }
}

void Kalk::setRealDomain() {
    iButton->setEnabled(false);
    jButton->setEnabled(false);
    kButton->setEnabled(false);
    squareRootButton->setEnabled(true);
    powerButton->setEnabled(true);

    isRealDomain = true;
    isComplexDomain = false;
    isQuaternionDomain = false;

    sumSoFar = new Real(sumSoFar->realVal());
    factorSoFar = new Real(factorSoFar->realVal());
}

void Kalk::setComplexDomain() {
    iButton->setEnabled(true);
    jButton->setEnabled(false);
    kButton->setEnabled(false);
    squareRootButton->setEnabled(true);
    powerButton->setEnabled(true);

    isRealDomain = false;
    isComplexDomain = true;
    isQuaternionDomain = false;

    sumSoFar = new Complex(sumSoFar->realVal(), sumSoFar->imaginaryVal());
    factorSoFar = new Complex(factorSoFar->realVal(), factorSoFar->imaginaryVal());
}

void Kalk::setQuaternionDomain() {
    iButton->setEnabled(true);
    jButton->setEnabled(true);
    kButton->setEnabled(true);
    squareRootButton->setEnabled(false);
    powerButton->setEnabled(false);

    isRealDomain = false;
    isComplexDomain = false;
    isQuaternionDomain = true;

    sumSoFar = new Quaternion(*sumSoFar);
    factorSoFar = new Quaternion(*factorSoFar);
}

void Kalk::digitClicked() {
    Button *clickedButton = qobject_cast<Button *>(sender());
    int digitValue = clickedButton->text().toInt();
    QString currentText = display->text();
    if (currentText == nullText && digitValue == 0.0) {
        return;
    }

    if (waitingForOperand) {
        display->clear();
        waitingForOperand = false;
    }

    QString newText = display->text();
    if (currentText.endsWith("i") || currentText.endsWith("j") || currentText.endsWith("k")) {
        if (newText == "") {
            newText = QString::number(digitValue);
        } else {
            newText += "+" + QString::number(digitValue);
        }
    } else {
        newText += QString::number(digitValue);
    }

    display->setText(newText);
}

Quaternion* Kalk::getOperand() const {
    std::string raw = display->text().toStdString();

    if (isRealDomain) {
        return new Real(raw);
    } else if (isComplexDomain) {
        return new Complex(raw);
    } else if (isQuaternionDomain) {
        return new Quaternion(raw);
    }

    return 0;
}

void Kalk::unaryOperatorClicked() {
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    Quaternion* operand = getOperand();

    if (clickedOperator == tr("i") || clickedOperator == tr("j") || clickedOperator == tr("k")) {
        QString newValue = display->text() + clickedOperator;
        display->setText(newValue);
        waitingForOperand = false;
        return;
    } else if (clickedOperator == tr("Sqrt")) {
        displayResult(operand->pow(0.5));
        waitingForOperand = true;
    } else if (clickedOperator == tr("1/x")) {
        displayResult(operand->pow(-1));
        waitingForOperand = true;
    }
}

void Kalk::additiveOperatorClicked() {
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    if (display->text().length() < 1) {  // +, - sign with no input
        display->setText(clickedOperator);
        return;
    }

    Quaternion* operand = getOperand();

    if (!pendingAdditiveOperator.isEmpty()) {
        if (!calculate(operand, pendingAdditiveOperator)) {
            abortOperation();
            return;
        }
        displayResult(sumSoFar);
    } else {
        sumSoFar = operand;
    }

    pendingAdditiveOperator = clickedOperator;
    waitingForOperand = true;
}

void Kalk::multiplicativeOperatorClicked() {
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    Quaternion* operand = getOperand();

    if (!pendingMultiplicativeOperator.isEmpty()) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
            return;
        }

        displayResult(factorSoFar);
    } else {
        factorSoFar = operand;
    }

    pendingMultiplicativeOperator = clickedOperator;
    waitingForOperand = true;
}

void Kalk::powerOperatorClicked() {
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    Quaternion* operand = getOperand();

    if (!pendingPowerOperator.isEmpty()) {
        if (!calculate(operand, pendingPowerOperator)) {
            abortOperation();
            return;
        }
        displayResult(factorSoFar);
    } else {
        factorSoFar = operand;
    }

    pendingPowerOperator = clickedOperator;
    waitingForOperand = true;
}

void Kalk::equalClicked() {
    Quaternion* operand = getOperand();
    if (!pendingMultiplicativeOperator.isEmpty()) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
            return;
        }
        operand = factorSoFar;
        factorSoFar = new Quaternion();
        pendingMultiplicativeOperator.clear();
    }

    if (!pendingPowerOperator.isEmpty()) {
        if (!calculate(operand, pendingPowerOperator)) {
            abortOperation();
            return;
        }
        operand = factorSoFar;
        factorSoFar = new Quaternion();
        pendingPowerOperator.clear();
    }

    if (!pendingAdditiveOperator.isEmpty()) {
        if (!calculate(operand, pendingAdditiveOperator)) {
            abortOperation();
            return;
        }
        pendingAdditiveOperator.clear();
    } else {
        sumSoFar = operand;
    }

    displayResult(sumSoFar);
    if (isRealDomain) {
        sumSoFar = new Real();
    } else if (isComplexDomain) {
        sumSoFar = new Complex();
    } else if (isQuaternionDomain) {
        sumSoFar = new Quaternion();
    }

    waitingForOperand = true;
}

void Kalk::pointClicked()
{
    if (waitingForOperand) {
        display->setText(nullText);
    }

    if (!display->text().contains('.')) {
        display->setText(display->text() + tr("."));
    }

    waitingForOperand = false;
}

void Kalk::backspaceClicked() {
    if (waitingForOperand)
        return;

    QString text = display->text();
    text.chop(1);
    if (text.isEmpty()) {
        text = nullText;
        waitingForOperand = true;
    }
    display->setText(text);
}

void Kalk::clear() {
    if (waitingForOperand) {
        return;
    }

    display->setText(nullText);
    waitingForOperand = true;
}

void Kalk::clearAll() {
    sumSoFar = new Quaternion();
    factorSoFar = new Quaternion();

    pendingAdditiveOperator.clear();
    pendingMultiplicativeOperator.clear();
    pendingPowerOperator.clear();

    display->setText(nullText);
    waitingForOperand = false;
}

Button *Kalk::createButton(const QString &text, const char *member) {
    Button *button = new Button(text);
    connect(button, SIGNAL(clicked()), this, member);
    return button;
}

void Kalk::abortOperation() {
    clearAll();
    display->setText(nullText);
}

bool Kalk::calculate(Quaternion* rightOperand, const QString &pendingOperator) {
    try {
        if (pendingOperator == tr("+")) {
            sumSoFar = sumSoFar->operator +(*rightOperand);
        } else if (pendingOperator == tr("-")) {
            sumSoFar = sumSoFar->operator -(*rightOperand);
        } else if (pendingOperator == tr("x")) {
            factorSoFar = factorSoFar->operator *(*rightOperand);
        } else if (pendingOperator == tr("x ^ y")) {
            factorSoFar = factorSoFar->pow(rightOperand->realVal());
        } else if (pendingOperator == tr("/")) {
            factorSoFar = factorSoFar->operator /(*rightOperand);
        }
        return true;
    } catch(...) {
        return false;
    }
}

void Kalk::displayResult(Quaternion* x) const {
    display->setText(QString::fromStdString(x->str()));
}
