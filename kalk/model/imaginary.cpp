/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "imaginary.h"
#include "../utils/utils.h"

Imaginary::Imaginary(double imaginary) : Complex(0, imaginary) {}

Imaginary::Imaginary(std::string raw): Complex(raw) {}

Real* Imaginary::operator*(const Quaternion & other) const {
    return new Real(imaginaryVal() * other.imaginaryVal());
}

Real* Imaginary::operator/(const Quaternion & other) const {
    return new Real(imaginaryVal() / other.imaginaryVal());
}

Imaginary & Imaginary::operator=(const Quaternion & other) {
    if (*this != other) {
        setImaginaryValue(other.imaginaryVal());
    }

    return *this;
}

bool Imaginary::operator==(const Quaternion & other) const {
    return areApproximatelyTheSame(imaginaryVal(), other.imaginaryVal(), epsilon);
}

std::string Imaginary::str() const {
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

std::ostream &operator<<(std::ostream& os, const Imaginary & x) {
    return os << x.imaginaryVal() << "i";
}

void Imaginary::fromString(std::string raw) {
    raw = trim_copy(raw);
    double currentSign = 1.0;
    bool hasStartedNumber = false;
    std::string currentNumber = "";

    for (unsigned int i = 0; i < raw.length(); i++) {
        char currentChar = raw[i];
        if (isdigit(currentChar) || currentChar == '.') {
            hasStartedNumber = true;
            currentNumber.push_back(currentChar);
        } else {  // it's a symbol
            if (!isalpha(currentChar)) {
                if (hasStartedNumber && currentNumber != "") {
                    currentNumber = "";  // erase
                    hasStartedNumber = false;
                }

                if (currentChar == '+') {
                    currentSign = 1.0;
                } else if (currentChar == '-') {
                    currentSign = -1.0;
                }
            } else {
                if (currentChar == 'i' && currentNumber != "") {
                    setImaginaryValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // reset
                    currentSign = 1.0;
                    hasStartedNumber = false;
                }
            }
        }
    }
}
