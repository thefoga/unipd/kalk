/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef REAL_H
#define REAL_H

#include "complex.h"

class Real : public Complex
{
public:
    Real(double=0);
    Real(std::string);

    // operations
    Real* pow(double) const;
    Real* reciprocal() const;

    // algebra operators
    virtual Real* operator+(const Quaternion &) const;
    virtual Real* operator-(const Quaternion &) const;
    virtual Real* operator*(const Quaternion &) const;
    virtual Real* operator/(const Quaternion &) const;

    // operators
    virtual Real &operator=(const Quaternion &);
    virtual bool operator==(const Quaternion &) const;

    // conversions
    virtual std::string str() const;
    friend std::ostream &operator<<(std::ostream&, const Real &);
    virtual void fromString(std::string);
};

#endif
