/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "quaternion.h"
#include "../utils/utils.h"

Quaternion::Quaternion(double real, double i, double j, double k) {
    setRealValue(real);
    setImaginaryValue(i);
    setJValue(j);
    setKValue(k);
}

Quaternion::Quaternion(std::string raw) {
    fromString(raw);
}

double Quaternion::realVal() const {
    return realValue;
}

double Quaternion::imaginaryVal() const {
    return imaginaryValue;
}

double Quaternion::jVal() const {
    return jValue;
}

double Quaternion::kVal() const {
    return kValue;
}

void Quaternion::setRealValue(double x) {
    realValue = fixValueEps(x, epsilon);
}

void Quaternion::setImaginaryValue(double x) {
    imaginaryValue = fixValueEps(x, epsilon);
}

void Quaternion::setJValue(double x) {
    jValue = fixValueEps(x, epsilon);
}

void Quaternion::setKValue(double x) {
    kValue = fixValueEps(x, epsilon);
}

void Quaternion::zero() {
    setRealValue(0);
    setImaginaryValue(0);
    setJValue(0);
    setKValue(0);
}

Quaternion* Quaternion::conjugate() const {
    return new Quaternion(realVal(), -imaginaryVal(), -jVal(), -kVal());
}

double Quaternion::abs() const {
    return std::pow(
        std::pow(realVal(), 2) +
        std::pow(imaginaryVal(), 2) +
        std::pow(jVal(), 2) +
        std::pow(kVal(), 2),
        0.5
    );
}

Quaternion* Quaternion::pow(double x) const {
    int exponent = std::round(x);
    Quaternion* result = new Quaternion(1, 1, 1, 1);
    for (int i = 0; i < exponent; i++) {
        result = result->operator *(*this);
    }
    return result;
}

Quaternion* Quaternion::operator+(const Quaternion & other) const {
    return new Quaternion(
        realVal() + other.realVal(),
        imaginaryVal() + other.imaginaryVal(),
        jVal() + other.jVal(),
        kVal() + other.kVal()
    );
}

Quaternion* Quaternion::operator-(const Quaternion & other) const {
    return new Quaternion(
        realVal() - other.realVal(),
        imaginaryVal() - other.imaginaryVal(),
        jVal() - other.jVal(),
        kVal() - other.kVal()
    );
}

Quaternion* Quaternion::operator*(const Quaternion & other) const {
    return new Quaternion(
        other.realVal() * realVal() - other.imaginaryVal() * imaginaryVal() - other.jVal() * jVal() - other.kVal() * kVal(),
        other.realVal() * imaginaryVal() + other.imaginaryVal() * realVal() - other.jVal() * kVal() + other.kVal() * jVal(),
        other.realVal() * jVal() + other.imaginaryVal() * kVal() + other.jVal() * realVal() - other.kVal() * imaginaryVal(),
        other.realVal() * kVal() - other.imaginaryVal() * jVal() + other.jVal() * imaginaryVal() + other.kVal() * realVal()
    );
}

Quaternion* Quaternion::operator/(const Quaternion & other) const {
    double squaredAbs = std::pow(other.abs(), 2.0);
    if (squaredAbs == 0.0) {
        return new Quaternion(INFINITY, INFINITY, INFINITY, INFINITY);
    }

    double realPart = other.realVal() * realVal() + other.imaginaryVal() * imaginaryVal() + other.jVal() * jVal() + other.kVal() * kVal();
    double iPart = other.realVal() * imaginaryVal() - other.imaginaryVal() * realVal() - other.jVal() * kVal() + other.kVal() * jVal();
    double jPart = other.realVal() * jVal() + other.imaginaryVal() * kVal() - other.jVal() * realVal() - other.kVal() * imaginaryVal();
    double kPart = other.realVal() * kVal() - other.imaginaryVal() * jVal() + other.jVal() * imaginaryVal() - other.kVal() * realVal();
    return new Quaternion(
        realPart / squaredAbs,
        iPart / squaredAbs,
        jPart / squaredAbs,
        kPart / squaredAbs
    );
}

bool Quaternion::operator<(const Quaternion & other) const {
    return abs() < other.abs();
}

bool Quaternion::operator>(const Quaternion & other) const {
    return (*this != other) && !(*this < other);
}

Quaternion & Quaternion::operator=(const Quaternion & other) {
    if (*this != other) {
        setRealValue(other.realVal());
        setImaginaryValue(other.imaginaryVal());
        setJValue(other.jVal());
        setKValue(other.kVal());
    }

    return *this;
}

bool Quaternion::operator==(const Quaternion & other) const {
    return areApproximatelyTheSame(realVal(), other.realVal(), epsilon) &&
            areApproximatelyTheSame(imaginaryVal(), other.imaginaryVal(), epsilon) &&
            areApproximatelyTheSame(jVal(), other.jVal(), epsilon) &&
            areApproximatelyTheSame(kVal(), other.kVal(), epsilon);
}

bool Quaternion::operator!=(const Quaternion & other) const {
    return !(*this == other);
}

std::string Quaternion::str() const {
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

std::ostream &operator<<(std::ostream& os, const Quaternion & x) {
    // real value
    os << x.realVal();

    // imaginary value
    if (x.imaginaryVal() < 0) {
        os << x.imaginaryVal();
    } else {
        os << "+" << x.imaginaryVal();
    }
    os << "i";

    // j value
    if (x.jVal() < 0) {
        os << x.jVal();
    } else {
        os << "+" << x.jVal();
    }
    os << "j";

    // k value
    if (x.kVal() < 0) {
        os << x.kVal();
    } else {
        os << "+" << x.kVal();
    }
    os << "k";
    return os;
}


void Quaternion::fromString(std::string raw) {
    zero();
    raw = trim_copy(raw);
    double currentSign = 1.0;
    bool hasStartedNumber = false;
    std::string currentNumber = "";

    for (unsigned int i = 0; i < raw.length(); i++) {
        char currentChar = raw[i];
        if (isdigit(currentChar) || currentChar == '.') {
            hasStartedNumber = true;
            currentNumber.push_back(currentChar);
        } else {  // it's a symbol
            if (!isalpha(currentChar)) {
                if (hasStartedNumber && currentNumber != "") {
                    setRealValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // erase
                    hasStartedNumber = false;
                }

                if (currentChar == '+') {
                    currentSign = 1.0;
                } else if (currentChar == '-') {
                    currentSign = -1.0;
                }
            } else {
                if (currentChar == 'i' && currentNumber != "") {
                    setImaginaryValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // reset
                    currentSign = 1.0;
                    hasStartedNumber = false;
                }

                if (currentChar == 'j' && currentNumber != "") {
                    setJValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // reset
                    currentSign = 1.0;
                    hasStartedNumber = false;
                }

                if (currentChar == 'k' && currentNumber != "") {
                    setKValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // reset
                    currentSign = 1.0;
                    hasStartedNumber = false;
                }
            }
        }
    }

    if (hasStartedNumber) {  // ended parsing without saving results
        setRealValue(currentSign * parseString(currentNumber));
    }
}

double Quaternion::epsilon = 0.0000001;
