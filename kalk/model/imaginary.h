/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef IMAGINARY_H
#define IMAGINARY_H

#include "complex.h"
#include "real.h"

class Imaginary : public Complex
{
public:
    Imaginary(double=0);
    Imaginary(std::string);

    // algebra operators
    virtual Real* operator*(const Quaternion &) const;
    virtual Real* operator/(const Quaternion &) const;

    // operators
    virtual Imaginary &operator=(const Quaternion &);
    virtual bool operator==(const Quaternion &) const;

    // conversions
    virtual std::string str() const;
    friend std::ostream &operator<<(std::ostream&, const Imaginary &);
    virtual void fromString(std::string);
};

#endif
