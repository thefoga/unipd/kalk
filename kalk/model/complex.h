/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef COMPLEX_H
#define COMPLEX_H

#include <cmath>
#include "quaternion.h"

class Complex : public Quaternion
{
public:
    Complex(double=0, double=0);
    Complex(std::string);

    // operations
    double polarAngle() const;
    virtual Complex* pow(double) const;

    // algebra operators
    virtual Complex* operator+(const Quaternion &) const;
    virtual Complex* operator-(const Quaternion &) const;
    virtual Complex* operator*(const Quaternion &) const;
    virtual Complex* operator/(const Quaternion &) const;

    // operators
    virtual Complex &operator=(const Quaternion &);
    virtual bool operator==(const Quaternion &) const;

    // conversions
    virtual std::string str() const;
    friend std::ostream &operator<<(std::ostream&, const Complex &);
    virtual void fromString(std::string);
};

#endif
