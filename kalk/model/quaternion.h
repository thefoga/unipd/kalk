/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef QUATERNION_H
#define QUATERNION_H

#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

class Quaternion
{
public:
    Quaternion(double=0, double=0, double=0, double=0);
    Quaternion(std::string);

    // getters
    double realVal() const;
    double imaginaryVal() const;
    double jVal() const;
    double kVal() const;

    // setters
    void setRealValue(double);
    void setImaginaryValue(double);
    void setJValue(double);
    void setKValue(double);

    // operations
    void zero();
    virtual Quaternion* conjugate() const;
    double abs() const;
    virtual Quaternion* pow(double) const;  // just with natural powers

    // algebra operators
    virtual Quaternion* operator+(const Quaternion &) const;
    virtual Quaternion* operator-(const Quaternion &) const;
    virtual Quaternion* operator*(const Quaternion &) const;
    virtual Quaternion* operator/(const Quaternion &) const;

    // comparisons
    bool operator<(const Quaternion &) const;
    bool operator>(const Quaternion &) const;

    // operators
    virtual Quaternion &operator=(const Quaternion &);
    virtual bool operator==(const Quaternion &) const;
    virtual bool operator!=(const Quaternion &) const;

    // conversions
    virtual std::string str() const;
    friend std::ostream &operator<<(std::ostream&, const Quaternion &);
    virtual void fromString(std::string);

    // static
    static double epsilon;
private:
    double realValue, imaginaryValue, jValue, kValue;
};

#endif
