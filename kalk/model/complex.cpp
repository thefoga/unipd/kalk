/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "complex.h"
#include "../utils/utils.h"

Complex::Complex(double real, double imaginary) : Quaternion(real, imaginary, 0, 0) {}

Complex::Complex(std::string raw): Quaternion(raw) {}

double Complex::polarAngle() const {
    if (imaginaryVal() == 0) {
        if (realVal() >= 0) {
            return 0.0;
        }

        return M_PI;
    } else if (realVal() == 0) {
        if (imaginaryVal() >= 0) {
            return M_PI / 2.0;
        }

        return M_PI * 3 / 2;
    }

    return std::atan(imaginaryVal() / realVal());
}

Complex* Complex::pow(double exponent) const {
    double r = abs();
    double rPowered = std::pow(r, exponent);
    double theta = polarAngle();
    return new Complex(
        rPowered * std::cos(exponent * theta),
        rPowered * std::sin(exponent * theta)
    );
}

Complex* Complex::operator+(const Quaternion & other) const {
    return new Complex(realVal() + other.realVal(), imaginaryVal() + other.imaginaryVal());
}

Complex* Complex::operator-(const Quaternion & other) const {
    return new Complex(realVal() - other.realVal(), imaginaryVal() - other.imaginaryVal());
}

Complex* Complex::operator*(const Quaternion & other) const {
    return new Complex(
        realVal() * other.realVal() - imaginaryVal() * other.imaginaryVal(),
        realVal() * other.imaginaryVal() + imaginaryVal() * other.realVal()
    );
}

Complex* Complex::operator/(const Quaternion & other) const {
    double squaredAbs = std::pow(other.abs(), 2.0);
    if (squaredAbs == 0.0) {
        return new Complex(INFINITY, INFINITY);
    }

    double realPart = realVal() * other.realVal() + imaginaryVal() * other.imaginaryVal();
    double imaginaryPart = imaginaryVal() * other.realVal() - realVal() * other.imaginaryVal();
    return new Complex(realPart / squaredAbs, imaginaryPart / squaredAbs);
}

Complex & Complex::operator=(const Quaternion & other) {
    if (*this != other) {
        setRealValue(other.realVal());
        setImaginaryValue(other.imaginaryVal());
    }

    return *this;
}

bool Complex::operator==(const Quaternion & other) const {
    return areApproximatelyTheSame(realVal(), other.realVal(), epsilon) &&
            areApproximatelyTheSame(imaginaryVal(), other.imaginaryVal(), epsilon);
}

std::string Complex::str() const {
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

std::ostream &operator<<(std::ostream& os, const Complex & x) {
    os << x.realVal();
    if (x.imaginaryVal() < 0) {
        os << x.imaginaryVal();
    } else {
        os << "+" << x.imaginaryVal();
    }

    return os << "i";
}

void Complex::fromString(std::string raw) {
    zero();
    raw = trim_copy(raw);
    double currentSign = 1.0;
    bool hasStartedNumber = false;
    std::string currentNumber = "";

    for (unsigned int i = 0; i < raw.length(); i++) {
        char currentChar = raw[i];
        if (isdigit(currentChar) || currentChar == '.') {
            hasStartedNumber = true;
            currentNumber.push_back(currentChar);
        } else {  // it's a symbol
            if (!isalpha(currentChar)) {
                if (hasStartedNumber && currentNumber != "") {
                    setRealValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // erase
                    hasStartedNumber = false;
                }

                if (currentChar == '+') {
                    currentSign = 1.0;
                } else if (currentChar == '-') {
                    currentSign = -1.0;
                }
            } else {
                if (currentChar == 'i' && currentNumber != "") {
                    std::cout << "parsed " << currentNumber << std::endl;
                    setImaginaryValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // reset
                    currentSign = 1.0;
                    hasStartedNumber = false;
                }
            }
        }
    }

    if (hasStartedNumber) {  // ended parsing without saving results
        setRealValue(currentSign * parseString(currentNumber));
    }
}
