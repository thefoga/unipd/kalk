/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "real.h"
#include "utils/utils.h"

Real::Real(double value) : Complex(value, 0) {
}

Real::Real(std::string raw): Complex(raw) {}

Real* Real::pow(double exponent) const {
    return new Real(std::pow(realVal(), exponent));
}

Real* Real::reciprocal() const {
    if (realVal() == 0) {
        return new Real(INFINITY);
    }

    return pow(-1.0);
}

Real* Real::operator+(const Quaternion & other) const {
    return new Real(realVal() + other.realVal());
}
Real* Real::operator-(const Quaternion & other) const {
    return new Real(realVal() - other.realVal());
}
Real* Real::operator*(const Quaternion & other) const {
    return new Real(realVal() * other.realVal());
}
Real* Real::operator/(const Quaternion & other) const {
    if (*this == other) {
        return new Real(1.0);
    }

    if (other.realVal() == 0.0) {
        return new Real(INFINITY);
    }

    return new Real(realVal() / other.realVal());
}

Real & Real::operator=(const Quaternion & other) {
    if (*this != other) {
        setRealValue(other.realVal());
    }

    return *this;
}

bool Real::operator==(const Quaternion & other) const {
    return areApproximatelyTheSame(realVal(), other.realVal(), epsilon);
}

std::string Real::str() const {
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

std::ostream &operator<<(std::ostream& os, const Real & x) {
    return os << x.realVal();
}

void Real::fromString(std::string raw) {
    zero();
    raw = trim_copy(raw);
    double currentSign = 1.0;
    bool hasStartedNumber = false;
    std::string currentNumber = "";

    for (unsigned int i = 0; i < raw.length(); i++) {
        char currentChar = raw[i];
        if (isdigit(currentChar) || currentChar == '.') {
            hasStartedNumber = true;
            currentNumber.push_back(currentChar);
        } else {  // it's a symbol
            if (!isalpha(currentChar)) {
                if (hasStartedNumber && currentNumber != "") {
                    setRealValue(currentSign * parseString(currentNumber));
                    currentNumber = "";  // erase
                    hasStartedNumber = false;
                }

                if (currentChar == '+') {
                    currentSign = 1.0;
                } else if (currentChar == '-') {
                    currentSign = -1.0;
                }
            } else {
                if (currentChar == 'i' && currentNumber != "") {
                    currentNumber = "";  // reset
                    currentSign = 1.0;
                    hasStartedNumber = false;
                }
            }
        }
    }

    if (hasStartedNumber) {  // ended parsing without saving results
        setRealValue(currentSign * parseString(currentNumber));
    }
}
