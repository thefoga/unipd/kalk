QT       += core gui

QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kalk
TEMPLATE = app

SOURCES += main.cpp \
    button.cpp \
    kalk.cpp \
    model/complex.cpp \
    model/imaginary.cpp \
    model/real.cpp \
    model/quaternion.cpp \
    utils/utils.cpp

HEADERS += \
    button.h \
    kalk.h \
    model/complex.h \
    model/real.h \
    model/imaginary.h \
    model/quaternion.h \
    tests/real.h \
    tests/quaternion.h \
    tests/imaginary.h \
    tests/complex.h \
    tests/test.h \
    utils/utils.h
