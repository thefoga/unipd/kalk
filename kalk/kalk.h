/**
 * Copyright 2018 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef KALK_H
#define KALK_H

#include <QWidget>
#include <QtWidgets>

#include "model/real.h"
#include "model/complex.h"
#include "model/quaternion.h"

QT_BEGIN_NAMESPACE
class QLineEdit;
QT_END_NAMESPACE
class Button;

class Kalk : public QWidget
{
    Q_OBJECT

public:
    Kalk(QWidget *parent = 0);

private slots:
    void digitClicked();
    void unaryOperatorClicked();
    void additiveOperatorClicked();
    void multiplicativeOperatorClicked();
    void powerOperatorClicked();
    void equalClicked();
    void pointClicked();
    void backspaceClicked();
    void clear();
    void clearAll();
    void setNumberDomain(int);

private:
    // GUI layout
    void setupDisplay();
    void setupLayout();
    QGridLayout *createLayout();

    // number domains
    void setRealDomain();
    void setComplexDomain();
    void setQuaternionDomain();
    bool isRealDomain, isComplexDomain, isQuaternionDomain;
    bool hasUsedReal, hasUsedI, hasUsedJ, hasUsedK;

    // buttons
    Button *squareRootButton, *powerButton;
    Button *createButton(const QString &text, const char *member);
    Button *iButton, *jButton, *kButton;

    // calculations
    Quaternion* getOperand() const;
    void abortOperation();
    bool calculate(Quaternion* rightOperand, const QString &pendingOperator);

    // operations
    Quaternion* sumSoFar;
    Quaternion* factorSoFar;
    QString pendingAdditiveOperator;
    QString pendingMultiplicativeOperator;
    QString pendingPowerOperator;
    bool waitingForOperand;

    // digit display
    void displayResult(Quaternion*) const;
    QLineEdit *display;
    QString nullText = "";
    enum { NumDigitButtons = 10 };
    Button *digitButtons[NumDigitButtons];
};

#endif
