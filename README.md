# Kalk

*OOP class project: a calculator for reals, imaginary and complex numbers*

[![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://opensource.org/licenses/Apache-2.0) [![Contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/sirfoga/kalk/issues) [![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)


## Compile
```shell
$ cd kalk
$ mkdir build
$ cd build
$ qmake ../kalk.pro
$ make
```


## Usage
```shell
$ ./Kalk  # to run the GUI
```


## Documentation
Doxygen has been used to compile documentation. You can find various format here:
- [html](docs/index.html) or the web version [here](https://sirfoga.github.io/kalk)
- [latex](docs/latex/Makefile)
- [rich text format](docs/rtf/refman.rtf)
- [xml](docs/xlm/index.xml)


## License
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
