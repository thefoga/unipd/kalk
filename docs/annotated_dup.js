var annotated_dup =
[
    [ "Button", "class_button.html", "class_button" ],
    [ "Complex", "class_complex.html", "class_complex" ],
    [ "ComplexTest", "class_complex_test.html", "class_complex_test" ],
    [ "Imaginary", "class_imaginary.html", "class_imaginary" ],
    [ "ImaginaryTest", "class_imaginary_test.html", "class_imaginary_test" ],
    [ "Kalk", "class_kalk.html", "class_kalk" ],
    [ "Quaternion", "class_quaternion.html", "class_quaternion" ],
    [ "QuaternionTest", "class_quaternion_test.html", "class_quaternion_test" ],
    [ "Real", "class_real.html", "class_real" ],
    [ "RealTest", "class_real_test.html", "class_real_test" ],
    [ "Test", "class_test.html", "class_test" ]
];