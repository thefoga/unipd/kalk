var class_imaginary =
[
    [ "Imaginary", "class_imaginary.html#ad01ee9acc5b2ebedc9f35aa7790fea99", null ],
    [ "Imaginary", "class_imaginary.html#a4f2d149a21dc2d90dd3786f50d58f23a", null ],
    [ "fromString", "class_imaginary.html#a6185504668486f8314e36f49f278f135", null ],
    [ "operator*", "class_imaginary.html#afbe877ac00c232313a6fcb787f478df1", null ],
    [ "operator/", "class_imaginary.html#a628cd2cb141e9f63a28e39f8651e690e", null ],
    [ "operator=", "class_imaginary.html#aadfbaa471778121b58ec2df740b3b632", null ],
    [ "operator==", "class_imaginary.html#aeec4923cf76a15efc947031f8255e23a", null ],
    [ "str", "class_imaginary.html#ae017d397ca21db325abbfda2b6107568", null ],
    [ "operator<<", "class_imaginary.html#a7b644b2980a41e3a81482bb0c5b351eb", null ]
];