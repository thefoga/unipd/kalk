var hierarchy =
[
    [ "QToolButton", null, [
      [ "Button", "class_button.html", null ]
    ] ],
    [ "Quaternion", "class_quaternion.html", [
      [ "Complex", "class_complex.html", [
        [ "Imaginary", "class_imaginary.html", null ],
        [ "Real", "class_real.html", null ]
      ] ]
    ] ],
    [ "QWidget", null, [
      [ "Kalk", "class_kalk.html", null ]
    ] ],
    [ "Test", "class_test.html", [
      [ "ComplexTest", "class_complex_test.html", null ],
      [ "ImaginaryTest", "class_imaginary_test.html", null ],
      [ "QuaternionTest", "class_quaternion_test.html", null ],
      [ "RealTest", "class_real_test.html", null ]
    ] ]
];